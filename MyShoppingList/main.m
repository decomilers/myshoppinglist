//
//  main.m
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 12/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
