//
//  ListViewController.h
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 12/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddItemViewController.h"
#import <CoreData/CoreData.h>

@interface ListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)clickedOnNewList;


@end

