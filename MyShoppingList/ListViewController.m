//
//  ListViewController.m
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 12/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import "ListViewController.h"

@interface ListViewController ()
{
    NSMutableArray *listObjArr;
}
@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //listArr = [[NSMutableArray alloc]init];
    //[listArr addObjectsFromArray:[NSArray arrayWithObjects:@"Woolworth",@"Coles",@"Indian Shop", nil]];
    
    self.tableView.tableFooterView = [UIView new];
    
    [self readDataRows];
    
    [self setupUI];

}
- (void)setupUI{
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =  @"Shopping List";
    self.navigationItem.titleView = titleLabel;
}
- (void)readDataRows{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"List"];
    listObjArr = [[context executeFetchRequest:request error:nil] mutableCopy];
    
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    //UIEdgeInsets inset = UIEdgeInsetsMake(10, 10, 10, 10);
    //self.tableView.contentInset = inset;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)clickedOnNewList{
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Create New List" message:@"eg. Woolworths, Coles" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        NSManagedObjectContext *context = [self managedObjectContext];
        NSManagedObject *newList = [NSEntityDescription insertNewObjectForEntityForName:@"List" inManagedObjectContext:context];
        
        NSString *primaryid = [[NSProcessInfo processInfo] globallyUniqueString];
        
        [newList setValue:primaryid forKey:@"listid"];
        [newList setValue:[alertView.textFields firstObject].text forKey:@"listname"];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        //[self readDataRows];
        //[self.tableView reloadData];
        [listObjArr addObject:newList];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:listObjArr.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        
    }];
    UIAlertAction *CANCEL = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        
    }];
    
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"List Name";
    }];
    
    [alertView addAction:OK];
    [alertView addAction:CANCEL];
    
    [self presentViewController:alertView animated:YES completion:nil];
}

#pragma mark - tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listObjArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSManagedObject *listObj = [listObjArr objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [listObj valueForKey:@"listname"];
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"gotoAddItem" sender:indexPath];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [context deleteObject:[listObjArr objectAtIndex:indexPath.row]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        // Remove device from table view
        [listObjArr removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    
    
    
    //[self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *indexPath = (NSIndexPath *)sender;
    
    if([segue.identifier isEqualToString:@"gotoAddItem"])
    {
        AddItemViewController *next = (AddItemViewController *)segue.destinationViewController;
        NSManagedObject *object = [listObjArr objectAtIndex:indexPath.row];
        next.listID = [object valueForKey:@"listid"];
        next.listname = [object valueForKey:@"listname"];
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}





@end
