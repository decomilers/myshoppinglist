//
//  CreateItemViewController.m
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import "CreateItemViewController.h"

@interface CreateItemViewController ()
{
    NSArray *catArray;
    UITapGestureRecognizer *tap;
}
@end

@implementation CreateItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setupTitlebar];
    
}
- (void)setupTitlebar{
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"Settings";
    self.navigationItem.titleView = titleLabel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupUI{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
    catArray = [context executeFetchRequest:request error:nil];
    
    
    self.picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 200)];
    self.picker.dataSource = self;
    self.picker.delegate = self;
    self.picker.showsSelectionIndicator = YES;
    [self.view addSubview:self.picker];
    
    self.tfdCategory.text = [[catArray objectAtIndex:0] valueForKey:@"categoryname"];
    
    if(self.name)
    {
        self.tfdName.text = self.name;
    }
    
}
- (IBAction)clickedOnAdd:(id)sender{
    
    if(![self.tfdCategory.text isEqualToString:@""] && ![self.tfdName.text isEqualToString:@""])
    {
        
        NSManagedObjectContext *context = [self managedObjectContext];
        
        //unique item
        NSManagedObject *uniqueObj = [NSEntityDescription insertNewObjectForEntityForName:@"UniqueItem" inManagedObjectContext:context];
        NSString *primaryUniid = [[NSProcessInfo processInfo] globallyUniqueString];
        NSString *categoryId = [[catArray objectAtIndex:[self.picker selectedRowInComponent:0]] valueForKey:@"categoryid"];
        
        [uniqueObj setValue:primaryUniid forKey:@"uniqueitemid"];
        [uniqueObj setValue:self.tfdName.text forKey:@"uniqueitemname"];
        [uniqueObj setValue:categoryId forKey:@"unqueitem_categoryid"];
        
        
        if(self.isFromAddItems)
        {
            //item in all Item table
            NSManagedObject *itemObject = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
            NSString *primaryid = [[NSProcessInfo processInfo] globallyUniqueString];
            [itemObject setValue:primaryid forKey:@"itemid"];
            [itemObject setValue:self.tfdName.text forKey:@"itemname"];
            [itemObject setValue:self.listid forKey:@"item_listid"];
            [itemObject setValue:categoryId forKey:@"item_categoryid"];
            [itemObject setValue:primaryUniid forKey:@"item_uniqueitemid"];
        }
        
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    
    }
    else
    {
        [[UIAlertView alloc]initWithTitle:@"Error" message:@"name or category cant be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
}
#pragma mark - pickerView Datasource and delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return catArray.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSManagedObject *obj = [catArray objectAtIndex:row];
    return [obj valueForKey:@"categoryname"];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.tfdCategory.text = [[catArray objectAtIndex:row] valueForKey:@"categoryname"];
}


#pragma mark - UItextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tappedElsewhere)];
    [self.view addGestureRecognizer:tap];
    
    if(textField == self.tfdCategory)
    {
        [self.view addSubview:self.picker];
        [self.view endEditing:YES];
    }
    else
    {
        //[self.picker removeFromSuperview];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.view removeGestureRecognizer:tap];
}

- (void)tappedElsewhere{
    [self.view endEditing:YES];
    //[self.tfdName resignFirstResponder];
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSManagedObjectContext *)managedObjectContext {
    
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
