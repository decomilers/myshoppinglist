//
//  SettingViewController.h
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SettingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
- (IBAction)clickedOnNewCategory:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCategory;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@end
