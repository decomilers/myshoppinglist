//
//  AddItemViewController.m
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import "AddItemViewController.h"
#import "ResultTableViewController.h"

@interface AddItemViewController ()
{
    NSMutableArray *allItemArr;
    NSMutableArray *searchResultArr;
    NSMutableArray *allUniqueItems;
    NSMutableArray *allCategory;
    
}
@property(strong,nonatomic) ResultTableViewController *resultViewController;
@property(strong,nonatomic)UISearchController *searchController;
@property(strong,nonatomic)UIButton *addNewItemBtn;
@property (strong,nonatomic)NSMutableDictionary *dict;
@end

@implementation AddItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setupUI];
    [self setupTitlebar];
}
- (void)setupTitlebar{
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =  self.listname;
    self.navigationItem.titleView = titleLabel;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [self readAllCategory];
    [self readAllItemsRow];
    
    [self readUniqueItems];
    
}
- (void)setupUI{
    
    searchResultArr = [[NSMutableArray alloc]init];
    
    self.resultViewController =[[ResultTableViewController alloc]init]; //(ResultTableViewController *)[[UITableViewController alloc]initWithStyle:UITableViewStylePlain];
    self.resultViewController.tableView.delegate = self;
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:self.resultViewController];
    self.searchController.delegate = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchResultsUpdater = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = true;
    
    //
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.resultViewController.tableView.bounds.size.width, 30)];
    //headerView.backgroundColor = [UIColor greenColor];
    self.addNewItemBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 8, self.resultViewController.tableView.bounds.size.width-40, 19)];
    [self.addNewItemBtn.titleLabel sizeToFit];
    [self.addNewItemBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.addNewItemBtn setTitle:@"add" forState:UIControlStateNormal];
    self.addNewItemBtn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    [self.addNewItemBtn addTarget:self action:@selector(clickedOnAddNewItem) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:self.addNewItemBtn];
    self.resultViewController.tableView.tableHeaderView = headerView;
 
    
    self.tableView.tableFooterView = [UIView new];
}
- (void)clickedOnAddNewItem{
    [self performSegueWithIdentifier:@"gotoCreateitem" sender:self.searchController.searchBar.text];
   //ad [self dismissViewControllerAnimated:self.searchController completion:nil];
}
- (void)readAllCategory{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Category"];
    allCategory = [context executeFetchRequest:request error:nil];
}
- (void)readAllItemsRow{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Item"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_listid = %@",self.listID];
    [request setPredicate:predicate];
    
    allItemArr = [[context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
    
    
    
    self.dict = [[NSMutableDictionary alloc]init];
    for(int i = 0; i < allCategory.count; i++)
    {
        NSObject *catObj = [allCategory objectAtIndex:i];
        NSString *catId = [catObj valueForKey:@"categoryid"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_categoryid = %@",catId];
        NSArray *arr = [allItemArr filteredArrayUsingPredicate:predicate];
        if(arr.count>0)
        {
            [self.dict setObject:arr forKey:catId];
        }
        else
        {
            //[self.dict setObject:nil forKey:catId];
        }
    }
    
    //NSLog(@"");
    
    
    
    
}
- (void)readUniqueItems{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"UniqueItem"];
    allUniqueItems = [[context executeFetchRequest:request error:nil] mutableCopy];
    self.resultViewController.tableArr = allUniqueItems;
    [self.resultViewController.tableView reloadData];
}
- (IBAction)clickedOnSearchBtn:(id)sender{
    [self presentViewController:self.searchController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UISearchBar delegate
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    // update the filtered array based on the search text
    NSString *searchString = searchController.searchBar.text;
    [self searchForText:searchString];
    if(searchResultArr.count > 0)
    {
        NSString *str = [NSString stringWithFormat:@"Create new item"];
        [self.addNewItemBtn setTitle:str forState:UIControlStateNormal];
        
        ResultTableViewController *tableController = (ResultTableViewController *)self.searchController.searchResultsController;
       tableController.tableArr = searchResultArr;
        [tableController.tableView reloadData];
    }
    else
    {
        NSString *str = [NSString stringWithFormat:@"Create new item - %@",searchString];
        [self.addNewItemBtn setTitle:str forState:UIControlStateNormal];
    }
    
}
- (void)searchForText:(NSString*)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uniqueitemname contains[c] %@", searchText];
    searchResultArr = [[allUniqueItems filteredArrayUsingPredicate:predicate] mutableCopy];
    NSLog(@"");
}

#pragma mark - table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr = [self.dict objectForKey:[[self.dict allKeys] objectAtIndex:section]];
    
    return arr.count;//allItemArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSArray *arr = [self.dict allKeys];
    return arr.count;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderCell"];
    
    
    if(self.dict.count >0)
    {
        UILabel *lbl = [cell.contentView viewWithTag:100];
        lbl.backgroundColor = [UIColor clearColor];
        NSString *catid = [[self.dict allKeys] objectAtIndex:section];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"categoryid = %@",catid];
        NSArray *arr = [allCategory filteredArrayUsingPredicate:predicate];
        lbl.text = [[arr firstObject] valueForKey:@"categoryname"];
        
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    return cell.contentView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 33;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}
- (CGFloat )tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *arr = [self.dict objectForKey:[[self.dict allKeys] objectAtIndex:indexPath.section]];
    
    NSManagedObject *itemObj = [arr objectAtIndex:indexPath.row];
    cell.textLabel.text = [itemObj valueForKey:@"itemname"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.tableView)
    {
        
    }
    else //search result
    {
        NSManagedObject *selectedObj = [searchResultArr objectAtIndex:indexPath.row];
        
        NSManagedObjectContext *context = [self managedObjectContext];
        
        //unique item
        //NSManagedObject *uniqueObj = [NSEntityDescription insertNewObjectForEntityForName:@"UniqueItem" inManagedObjectContext:context];
        //NSString *primaryUniid = [[NSProcessInfo processInfo] globallyUniqueString];
        //NSString *categoryId = [[catArray objectAtIndex:[self.picker selectedRowInComponent:0]] valueForKey:@"categoryid"];
        
        //[uniqueObj setValue:primaryUniid forKey:@"uniqueitemid"];
        //[uniqueObj setValue:self.tfdName.text forKey:@"uniqueitemname"];
        //[uniqueObj setValue:categoryId forKey:@"unqueitem_categoryid"];
        
        //item in all Item table
        NSManagedObject *itemObject = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:context];
        NSString *primaryid = [[NSProcessInfo processInfo] globallyUniqueString];
        [itemObject setValue:primaryid forKey:@"itemid"];
        [itemObject setValue:[selectedObj valueForKey:@"uniqueitemname"] forKey:@"itemname"];
        [itemObject setValue:self.listID forKey:@"item_listid"];
        [itemObject setValue:[selectedObj valueForKey:@"unqueitem_categoryid"] forKey:@"item_categoryid"];
        [itemObject setValue:[selectedObj valueForKey:@"uniqueitemid"] forKey:@"item_uniqueitemid"];
        
        
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        
        [searchResultArr removeObjectAtIndex:indexPath.row];
        self.resultViewController.tableArr = searchResultArr;
        [self.resultViewController.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
        
        //[self readAllCategory];
        [self readAllItemsRow];
        
        //[self readUniqueItems];
        [self.tableView reloadData];
        
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return true;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        NSMutableArray *arr = [[self.dict objectForKey:[[self.dict allKeys] objectAtIndex:indexPath.section]] mutableCopy];
        NSManagedObject *itemObj = [arr objectAtIndex:indexPath.row];
        [context deleteObject:itemObj];
        
        [arr removeObjectAtIndex:indexPath.row];
        [self.dict setObject:arr forKey:[[self.dict allKeys] objectAtIndex:indexPath.section]];
        
        
        NSError *error = nil;
        if (![context save:&error]) {
            
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        // Remove device from table view
        
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [allItemArr removeObject:itemObj];
    }

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"gotoCreateitem"])
    {
        NSString *name = (NSString *)sender;
        CreateItemViewController *next = (CreateItemViewController *)segue.destinationViewController;
        next.name = name;
        next.listid = self.listID;
        next.isFromAddItems = YES;
    }
    
}
- (NSManagedObjectContext *)managedObjectContext {
    
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
