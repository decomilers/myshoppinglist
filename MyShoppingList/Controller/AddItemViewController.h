//
//  AddItemViewController.h
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "CreateItemViewController.h"

@interface AddItemViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchControllerDelegate,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSString *listID;
@property(nonatomic,strong)NSString *listname;
- (IBAction)clickedOnSearchBtn:(id)sender;
@end
