//
//  ResultTableViewController.h
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ResultTableViewController : UITableViewController

@property(strong,nonatomic) NSArray *tableArr;
@end
