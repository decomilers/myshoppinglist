//
//  CreateListVC.h
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 12/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateListVC : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
{
    
}

- (IBAction)clickedOnSelect:(id)sender;
@end
