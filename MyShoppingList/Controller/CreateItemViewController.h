//
//  CreateItemViewController.h
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface CreateItemViewController : UIViewController<UIPickerViewDelegate,UITextFieldDelegate>
@property(strong,nonatomic)NSString *name;
@property(strong,nonatomic)NSString *listid;
@property(nonatomic,assign)BOOL isFromAddItems;

@property (weak, nonatomic) IBOutlet UITextField *tfdName;
@property (weak, nonatomic) IBOutlet UITextField *tfdCategory;
@property(strong,nonatomic)UIPickerView *picker;
- (IBAction)clickedOnAdd:(id)sender;

@end
