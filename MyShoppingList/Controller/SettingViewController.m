//
//  SettingViewController.m
//  MyShoppingList
//
//  Created by Kaushal Elsewhere on 13/03/16.
//  Copyright © 2016 elsewhere. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableview.tableFooterView = [UIView new];
    [self setupTitlebar];
}
- (void)setupTitlebar{
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 30)];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"Settings";
    self.navigationItem.titleView = titleLabel;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Category"];
    NSArray *arr = [[context executeFetchRequest:request error:nil] mutableCopy];
    int count = 0;
    if(arr.count)
    {
        count = (int)arr.count;
    }
    
    NSString *text = [NSString stringWithFormat:@"(%d categories) Create new Category",count];
    self.btnAddCategory.titleLabel.text = text;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickedOnNewCategory{
    
    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Create New Category" message:@"eg. fruit, vegetable" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        NSManagedObjectContext *context = [self managedObjectContext];
        NSManagedObject *newList = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:context];
        

        NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Category"];
        NSArray *arr = [[context executeFetchRequest:request error:nil] mutableCopy];

        
        NSString *primaryid = [[NSProcessInfo processInfo] globallyUniqueString];
        
        [newList setValue:primaryid forKey:@"categoryid"];
        [newList setValue:[alertView.textFields firstObject].text forKey:@"categoryname"];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        else
        {
            self.btnAddCategory.titleLabel.text = [NSString stringWithFormat:@"(%d categories) Create new Category",(int)arr.count];
        }
        
        NSString *str= [NSString stringWithFormat:@"%@ is added successfully",[alertView.textFields firstObject].text];
        [[[UIAlertView alloc]initWithTitle:@"Success" message:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        
        
    }];
    UIAlertAction *CANCEL = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        
        
    }];
    
    [alertView addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"List Name";
    }];
    
    [alertView addAction:OK];
    [alertView addAction:CANCEL];
    
    [self presentViewController:alertView animated:YES completion:nil];


}
#pragma mark - tableview delegate and datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    if(indexPath.row == 0)
    {
        cell.textLabel.text = @"Create New Category";
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Create New Item";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row == 0)
    {
        [self clickedOnNewCategory];
    }
    else if(indexPath.row == 1)
    {
        [self performSegueWithIdentifier:@"gotoCreateItemView" sender:nil];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSManagedObjectContext *)managedObjectContext {
    
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
